package ru.tsc.goloshchapov.tm.component;

import ru.tsc.goloshchapov.tm.api.controller.ICommandController;
import ru.tsc.goloshchapov.tm.api.controller.IProjectController;
import ru.tsc.goloshchapov.tm.api.controller.ITaskController;
import ru.tsc.goloshchapov.tm.api.repository.ICommandRepository;
import ru.tsc.goloshchapov.tm.api.repository.IProjectRepository;
import ru.tsc.goloshchapov.tm.api.repository.ITaskRepository;
import ru.tsc.goloshchapov.tm.api.service.ICommandService;
import ru.tsc.goloshchapov.tm.api.service.IProjectService;
import ru.tsc.goloshchapov.tm.api.service.ITaskService;
import ru.tsc.goloshchapov.tm.constant.ArgumentConst;
import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.controller.CommandController;
import ru.tsc.goloshchapov.tm.controller.ProjectController;
import ru.tsc.goloshchapov.tm.controller.TaskController;
import ru.tsc.goloshchapov.tm.repository.CommandRepository;
import ru.tsc.goloshchapov.tm.repository.ProjectRepository;
import ru.tsc.goloshchapov.tm.repository.TaskRepository;
import ru.tsc.goloshchapov.tm.service.CommandService;
import ru.tsc.goloshchapov.tm.service.ProjectService;
import ru.tsc.goloshchapov.tm.service.TaskService;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void start(String[] args) {
        commandController.showWelcome();
        parseArgs(args);
        process();
    }

    public void process() {
        while (true) {
            commandController.showEnterCommand();
            parseCommand(TerminalUtil.nextLine());
        }
    }

    public void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        commandController.exit();
    }

}
