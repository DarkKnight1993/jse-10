package ru.tsc.goloshchapov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

}
